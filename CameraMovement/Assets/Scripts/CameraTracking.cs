﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracking : MonoBehaviour
{
    public bool perfectLookAt = false;
    public Transform trackedObject; // object the camera follows / looks at / rotates around
    public float speed = 2f; // free look rotation speed
    public float cameraResetSpeed = 5f;

    bool freeLook = false; // state that allows camera to be rotated around the tracked object
    Vector3 defaultOffset; // default vector from player to the camera position
    float startingVerticalPos;
    float defaultDistance; // default distance of player to camera

    // Start is called before the first frame update
    void Start()
    {
        defaultOffset = transform.position - trackedObject.position;
        defaultDistance = defaultOffset.magnitude;
        startingVerticalPos = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            perfectLookAt = !perfectLookAt;
        }
        HandleInput();

        if (freeLook)
        {
            FreeLook();
        }
        else
        {
            DefaultTracking();
        }

        MaintainRelativePosition();
    }

    void HandleInput()
    {
        if(Input.GetMouseButtonDown(2)) // on middle mouse down
        {
            freeLook = true; // Enable freelook
        }

        if (Input.GetMouseButtonUp(2)) // on middle mouse up
        {
            freeLook = false; // Disable freelook
        }
    }

    void DefaultTracking()
    {
        // maintain default offset from player, regardless of players world position
        Vector3 targetPosition = trackedObject.position + defaultOffset;
        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * cameraResetSpeed);
    }

    void FreeLook()
    {
        // calculate movement for camera from the mouses movement on screen as world moves based on the cameras relative position
        Vector3 horizontalMovement = transform.right * MouseTracker.mouseDelta.x;
        Vector3 verticalMovement = transform.up * MouseTracker.mouseDelta.y;

        // apply the calculated movement
        transform.position += (horizontalMovement + verticalMovement) * Time.deltaTime * speed;        
    }

    void MaintainRelativePosition()
    {
        Vector3 trackableToCamera = transform.position - trackedObject.position; // get the new vector from tracked object to camera  
        if (trackableToCamera.magnitude != defaultDistance) // if this has moved the camera outside of the allowed distance
        {
            // keep relative direction of camera from tracked object, but move camera the correct distance away
            transform.position = trackedObject.position + trackableToCamera.normalized * defaultDistance;
        }

        Vector3 lookAtOffset = Vector3.zero;
        // add offset to lookat location to stabalize camera during lerp
        if (perfectLookAt) lookAtOffset = new Vector3(0f, transform.position.y - startingVerticalPos, 0f); 

        // always face camera towards tracked object
        transform.LookAt(trackedObject.transform.position + lookAtOffset);
    }    
}
