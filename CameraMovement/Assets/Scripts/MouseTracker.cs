﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTracker : MonoBehaviour
{
    Vector3 lastMouse = Vector3.zero;
    public static Vector3 mouseDelta = Vector3.zero; // change in mouse position since last update
    // Start is called before the first frame update
    void Start()
    {
        lastMouse = Input.mousePosition;
    }

    // Update is called once per frame
    void Update()
    {
        MouseMovement();
    }

    void MouseMovement()
    {
        mouseDelta = lastMouse - Input.mousePosition;
        lastMouse = Input.mousePosition;
    }
}
